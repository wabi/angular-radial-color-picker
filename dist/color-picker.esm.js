ColorPickerService.$inject = ['$rootScope'];
function ColorPickerService($rootScope) {
    this.publish = publish;
    this.subscribe = subscribe;
    this.unsubscribe = unsubscribe;

    this.isKey = {
        up: function(key) { return key === 'Up' || key === 'ArrowUp' },
        down: function(key) { return key === 'Down' || key === 'ArrowDown' },
        left: function(key) { return key === 'Left' || key === 'ArrowLeft' },
        right: function(key) { return key === 'Right' || key === 'ArrowRight' },
        enter: function(key) { return key === 'Enter' }
    };

    this.rgbToHex = rgbToHex;
    this.rgbToHsl = rgbToHsl;
    this.hslToRgb = hslToRgb;
    this.hslToHex = hslToHex;

    var subscribers = [];
    var eventPrefix = 'color-picker.';

    /**
     * Broadcasts an event on the whole app. Uses $rootScope as an event bus
     * so that sibling components can catch the event also. $applyAsync is used
     * to make sure that the broadcast happens on the earliest next digest cycle.
     *
     * @memberOf Utilities
     *
     * @param  {string} eventName Sub-topic to broadcast
     * @param  {*}      [data]    Any type of payload to broadcast
     *
     * @return {void}
     */
    function publish(eventName, data) {
        $rootScope.$applyAsync(function() {
            // queue up the broadcast in the next digest
            $rootScope.$broadcast(eventPrefix + eventName, data);
        });
    }

    /**
     * Facade wrapper for outside world interaction via $rootScope events.
     *
     * @memberOf Utilities
     *
     * @param  {string}   eventName Sub-topic to listen for. Can be 'open' or 'close'
     * @param  {Function} callback  Function to invoke when event gets fired
     *
     * @return {Function}           Unsubscribe function for manual unsubscribtion.
     */
    function subscribe(eventName, callback) {
        var unsubToken = $rootScope.$on(eventPrefix + eventName, callback);

        subscribers.push(unsubToken);

        return unsubToken;
    }

    /**
     * Removes all event listeners setup with the `.subscribe()` method
     *
     * @memberOf Utilities
     *
     * @param  {string}   eventName Sub-topic to listen for. Can be 'open' or 'close'
     * @param  {Function} callback  Function to invoke when event gets fired
     *
     * @return {void}
     */
    function unsubscribe() {
        subscribers.forEach(function(unsubscribe) {
            unsubscribe();
        });
    }

    /**
     * Converts RGB color model to hexadecimal string.
     *
     * @memberOf Utilities
     *
     * @param  {number} r Integer between 0 and 255
     * @param  {number} g Integer between 0 and 255
     * @param  {number} b Integer between 0 and 255
     *
     * @return {string}   6 char long hex string
     */
    function rgbToHex(r, g, b) {
        return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }

    /**
     * Converts RGB color model to HSL model.
     *
     * @memberOf Utilities
     *
     * @param   {number} r Integer between 0 and 255
     * @param   {number} g Integer between 0 and 255
     * @param   {number} b Integer between 0 and 255
     *
     * @return  {Object}   The HSL representation containing the hue (in degrees),
     *                     saturation (in percentage) and luminosity (in percentage) fields.
     */
    function rgbToHsl(r, g, b) {
        r = r / 255;
        g = g / 255;
        b = b / 255;

        var h, s;
        var max = Math.max(r, g, b);
        var min = Math.min(r, g, b);
        var l = (max + min) / 2;

        if (max === min) {
            h = s = 0; // achromatic
        } else {
            var d = max - min;

            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

            if (max === r) h = (g - b) / d + (g < b ? 6 : 0);
            if (max === g) h = (b - r) / d + 2;
            if (max === b) h = (r - g) / d + 4;
        }

        return {
            hue: h * 60,
            saturation: s * 100,
            luminosity: l * 100
        };
    }

    /**
     * Converts HSL color model to hexademical string.
     *
     * @memberOf Utilities
     *
     * @param  {number} r Integer between 0 and 255
     * @param  {number} g Integer between 0 and 255
     * @param  {number} b Integer between 0 and 255
     *
     * @return {string}   6 char long hex string
     */
    function hslToHex(h, s, l) {
        var colorModel = hslToRgb(h, s, l);

        return rgbToHex(colorModel.red, colorModel.green, colorModel.blue);
    }

    /**
     * Converts HSL color model to RGB model.
     * Shamelessly taken from http://axonflux.com/handy-rgb-to-hsl-and-rgb-to-hsv-color-model-c
     *
     * @memberOf Utilities
     *
     * @param   {number} h The hue. Number in the 0-360 range
     * @param   {number} s The saturation. Number in the 0-100 range
     * @param   {number} l The luminosity. Number in the 0-100 range
     *
     * @return  {Object}   The RGB representation containing the red, green and blue fields
     */
    function hslToRgb(h, s, l) {
        var r, g, b;

        h = h / 360;
        s = s / 100;
        l = l / 100;

        if (s === 0) {
            r = g = b = l; // achromatic
        } else {
            var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
            var p = 2 * l - q;

            r = _hue2rgb(p, q, h + 1/3);
            g = _hue2rgb(p, q, h);
            b = _hue2rgb(p, q, h - 1/3);
        }

        return {
            red: Math.round(r * 255),
            green: Math.round(g * 255),
            blue: Math.round(b * 255)
        };
    }

    function _hue2rgb(p, q, t) {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1/6) return p + (q - p) * 6 * t;
        if (t < 1/2) return q;
        if (t < 2/3) return p + (q - p) * (2/3 - t) * 6;

        return p;
    }
}

/**
 * Modified version of Denis Radin's Propeller.
 *
 * @link https://github.com/PixelsCommander/Propeller
 */
function Propeller(element, options) {
    this.last = 0;
    this.active = false;
    this.element = element;
    this.update = this.update.bind(this);

    this.initAngleGetterSetter();
    this.initOptions(options);
    this.initHardwareAcceleration();
    this.bindHandlers();
    this.addListeners();
    this.update();
}

Propeller.prototype.initAngleGetterSetter = function () {
    Object.defineProperty(this, 'angle', {
        get: function () {
            return this.normalizeAngle(this._angle * Propeller.radToDegMulti);
        },
        set: function (value) {
            var rads = value * Propeller.degToRadMulti;

            this._angle = rads;
            this.virtualAngle = rads;
            this.updateCSS();
        }
    });
};

Propeller.prototype.normalizeAngle = function(angle) {
    var result = angle % 360;

    if (result < 0) {
        result = 360 + result;
    }

    return result;
};

Propeller.prototype.initOptions = function (options) {
    var defaults = {
        angle: 0,
        speed: 0,
        inertia: 0,
        minimalSpeed: 0.0087, // in rad, (1/2 degree)
        minimalAngleChange: 0.0087, // in rad, (1/2 degree)
    };

    options = options || defaults;

    this.onRotate = options.onRotate;
    this.onStop = options.onStop;
    this.onDragStop = options.onDragStop;
    this.onDragStart = options.onDragStart;

    this.angle = options.angle || defaults.angle;
    this.speed = (options.speed * Propeller.degToRadMulti) || defaults.speed;

    this.inertia = options.inertia || defaults.inertia;
    this.minimalSpeed = options.minimalSpeed || defaults.minimalSpeed;
    this.lastAppliedAngle = this.virtualAngle = this._angle = (options.angle * Propeller.degToRadMulti) || defaults.angle;
    this.minimalAngleChange = options.minimalAngleChange || defaults.minimalAngleChange;
};

Propeller.prototype.bindHandlers = function () {
    this.onRotationStart = this.onRotationStart.bind(this);
    this.onRotationStop = this.onRotationStop.bind(this);
    this.onRotated = this.onRotated.bind(this);
};

Propeller.prototype.addListeners = function () {
    this.listenersInstalled = true;

    this.element.addEventListener('touchstart', this.onRotationStart, { passive: true });
    this.element.addEventListener('touchmove', this.onRotated);
    this.element.addEventListener('touchend', this.onRotationStop, { passive: true });
    this.element.addEventListener('touchcancel', this.onRotationStop, { passive: true });

    this.element.addEventListener('mousedown', this.onRotationStart, { passive: true });
    this.element.addEventListener('mousemove', this.onRotated);
    this.element.addEventListener('mouseup', this.onRotationStop, { passive: true });
    this.element.addEventListener('mouseleave', this.onRotationStop);
};

Propeller.prototype.removeListeners = function () {
    this.listenersInstalled = false;

    this.element.removeEventListener('touchstart', this.onRotationStart);
    this.element.removeEventListener('touchmove', this.onRotated);
    this.element.removeEventListener('touchend', this.onRotationStop);
    this.element.removeEventListener('touchcancel', this.onRotationStop);

    this.element.removeEventListener('mousedown', this.onRotationStart);
    this.element.removeEventListener('mousemove', this.onRotated);
    this.element.removeEventListener('mouseup', this.onRotationStop);
    this.element.removeEventListener('mouseleave', this.onRotationStop);
};

Propeller.prototype.bind = function () {
    if (this.listenersInstalled !== true) {
        this.addListeners();
    }
};

Propeller.prototype.unbind = function () {
    if (this.listenersInstalled === true) {
        this.removeListeners();
        this.onRotationStop();
    }
};

Propeller.prototype.stop = function () {
    this.speed = 0;
    this.onRotationStop();
};

Propeller.prototype.onRotationStart = function (event) {
    this.initDrag();
    this.active = true;

    if (this.onDragStart !== undefined) {
        this.onDragStart(event);
    }
};

Propeller.prototype.onRotationStop = function (event) {
    if (this.onDragStop !== undefined && this.active === true) {
        this.active = false;
        this.onDragStop();
    }

    this.active = false;
};

Propeller.prototype.onRotated = function (event) {
    event.preventDefault();

    if (this.active === true) {
        if (event.targetTouches !== undefined && event.targetTouches[0] !== undefined) {
            this.lastMouseEvent = {
                x: event.targetTouches[0].clientX,
                y: event.targetTouches[0].clientY,
            };
        } else {
            this.lastMouseEvent = {
                x: event.clientX,
                y: event.clientY,
            };
        }
    }
};

Propeller.prototype.update = function (now) {
    // Calculating angle on requestAnimationFrame only for optimisation purposes
    // 8ms is roughly 120fps
    if (!this.last || now - this.last >= 8) {
        this.last = now;

        if (this.lastMouseEvent !== undefined && this.active === true) {
            this.updateAngleToMouse(this.lastMouseEvent);
        }

        this._angle = this.virtualAngle;
        this.applySpeed();
        this.applyInertia();

        // Update rotation until the angle difference between prev and current angle is lower than the minimal angle change
        if (Math.abs(this.lastAppliedAngle - this._angle) >= this.minimalAngleChange) {
            this.updateCSS();

            if (this.onRotate !== undefined) {
                this.onRotate();
            }

            this.lastAppliedAngle = this._angle;
        }
    }

    window.requestAnimationFrame(this.update);
};

Propeller.prototype.applySpeed = function () {
    if (this.inertia > 0 && this.speed !== 0 && this.active === false) {
        this.virtualAngle += this.speed;
    }
};

Propeller.prototype.applyInertia = function () {
    if (this.inertia > 0) {
        if (Math.abs(this.speed) >= this.minimalSpeed) {
            this.speed = this.speed * this.inertia;

            // Execute onStop callback when speed is less than the given threshold
            if (this.onStop !== undefined && this.active === false && Math.abs(this.speed) < this.minimalSpeed) {
                this.onStop();
            }
        } else {
            // Stop rotation when rotation speed gets below a given threshold
            this.speed = 0;
        }
    }
};

Propeller.prototype.getAngleToMouse = function (newPoint) {
    var rect = this.element.getBoundingClientRect();
    var center = {
        x: rect.left + (rect.width / 2),
        y: rect.top + (rect.height / 2),
    };

    // atan2 gives values between -180 to 180 deg
    // offset the angle by 90 degrees so that it's 0 to 360 deg
    return Math.atan2(newPoint.y - center.y, newPoint.x - center.x) + Propeller.rightAngleInRadians;
};

Propeller.prototype.updateAngleToMouse = function (newPoint) {
    var newMouseAngle = this.getAngleToMouse(newPoint);

    if (this.lastMouseAngle === undefined) {
        this.lastElementAngle = this.virtualAngle;
        this.lastMouseAngle = newMouseAngle;
    }

    var oldAngle = this.virtualAngle;
    this.mouseDiff = newMouseAngle - this.lastMouseAngle;
    this.virtualAngle = this.lastElementAngle + this.mouseDiff;
    this.speed = this.differenceBetweenAngles(this.virtualAngle, oldAngle);
};

Propeller.prototype.differenceBetweenAngles = function (newAngle, oldAngle) {
    var delta = newAngle - oldAngle;
    var radians = Math.atan2(Math.sin(delta), Math.cos(delta));

    return Math.round(radians * 1000) / 1000;
};

Propeller.prototype.initDrag = function () {
    this.speed = 0;
    this.lastMouseAngle = undefined;
    this.lastElementAngle = undefined;
    this.lastMouseEvent = undefined;
};

Propeller.prototype.initHardwareAcceleration = function () {
    this.element.style.willChange = 'transform';
    this.updateCSS();
};

Propeller.prototype.updateCSS = function () {
    this.element.style.transform = 'rotate(' + this._angle + 'rad) ';
};

Propeller.radToDegMulti = 57.29577951308232; // 180 / Math.PI
Propeller.degToRadMulti = 0.017453292519943295; // Math.PI / 180
Propeller.rightAngleInRadians = 1.5707963267948966; // Math.PI / 2

colorPickerRotator.$inject = ['ColorPickerService'];
function colorPickerRotator(ColorPickerService) {
    var directive = {
        link: ColorPickerRotatorLink,
        restrict: 'A',
        scope: {
            onRotate: '&',
            angle: '<',
            isDisabled: '<',
            mouseScroll: '<',
            scrollSensitivity: '<'
        }
    };

    return directive;

    function ColorPickerRotatorLink($scope, $element) {
        var colorPicker = $element[0].parentElement;
        var scrollSensitivity = $scope.scrollSensitivity || 2;
        var initialAngle = $scope.angle || 0;

        var propelInstance = new Propeller($element[0], {
            angle: initialAngle,
            inertia: .7,
            speed: 0,
            onRotate: function() {
                $scope.onRotate({ angle: this.angle });
            },
            onDragStart: function() {
                if (!$scope.isDisabled) {
                    $element.addClass('dragging');
                }
            },
            onDragStop: function() {
                if (!$scope.isDisabled) {
                    $element.removeClass('dragging');
                }
            }
        });

        colorPicker.addEventListener('keydown', onKeydown);
        colorPicker.addEventListener('dblclick', onDblClick, { passive: true });

        if ($scope.mouseScroll) {
            colorPicker.addEventListener('wheel', onScroll);
        }

        $scope.$watch('angle', function(newAngle, oldAngle) {
            if (newAngle !== oldAngle) {
                propelInstance.angle = newAngle;
            }
        });

        $scope.$on('$destroy', function() {
            propelInstance.stop();
            propelInstance.unbind();
            propelInstance = null;

            colorPicker.removeEventListener('keydown', onKeydown);
            colorPicker.removeEventListener('dblclick', onDblClick);
            colorPicker.removeEventListener('wheel', onScroll);

            colorPicker = null;
        });

        function onKeydown(ev) {
            if ($scope.isDisabled)
                return;

            var multiplier = 0;
            var isIncrementing = ColorPickerService.isKey.up(ev.key) || ColorPickerService.isKey.right(ev.key);
            var isDecrementing = ColorPickerService.isKey.down(ev.key) || ColorPickerService.isKey.left(ev.key);

            if (isIncrementing) {
                multiplier = 1;

                if (ev.ctrlKey) {
                    multiplier = 6;
                } else if (ev.shiftKey) {
                    multiplier = 3;
                }
            } else if (isDecrementing) {
                multiplier = -1;

                if (ev.ctrlKey) {
                    multiplier = -6;
                } else if (ev.shiftKey) {
                    multiplier = -3;
                }
            }

            if (isIncrementing || isDecrementing) {
                ev.preventDefault();
                propelInstance.angle += scrollSensitivity * multiplier;
            }
        }

        function onDblClick(ev) {
            if ($scope.isDisabled || !ev.target.classList.contains('rotator'))
                return;

            var newAngle = propelInstance.getAngleToMouse({
                x: ev.clientX,
                y: ev.clientY,
            });

            propelInstance.angle = newAngle * Propeller.radToDegMulti;
        }

        function onScroll(ev) {
            if ($scope.isDisabled)
                return;

            ev.preventDefault();

            if (ev.deltaY > 0) {
                propelInstance.angle += scrollSensitivity;
            } else {
                propelInstance.angle -= scrollSensitivity;
            }
        }
    }
}

/**
 * Modified version of Lea Verou's conic-gradient.
 *
 * @link https://github.com/leaverou/conic-gradient
 */
function ConicGradient(size) {
    this.canvas = document.createElement('canvas');
    this.context = this.canvas.getContext('2d');
    this.size = size;
    this.canvas.width = this.canvas.height = this.size;
    this.from = 0;
    this.stops = [
        { color: [255, 0, 0, 1], pos: 0 },
        { color: [255, 255, 0, 1], pos: 0.16666666666666666 },
        { color: [0, 255, 0, 1], pos: 0.33333333333333337 },
        { color: [0, 255, 255, 1], pos: 0.5 },
        { color: [0, 0, 255, 1], pos: 0.6666666666666666 },
        { color: [255, 0, 255, 1], pos: 0.8333333333333333 },
        { color: [255, 0, 0, 1], pos: 1 },
    ];
    this.paint();
}

ConicGradient.eps = 0.00001;
ConicGradient.deg = Math.PI / 180;

ConicGradient.prototype = {
    toString: function() {
        return 'url("' + this.dataURL + '")';
    },

    get dataURL() {
        // IE/Edge only render data-URI based background-image when the image data
        // is escaped.
        // Ref: https://developer.microsoft.com/en-us/microsoft-edge/platform/issues/7157459/#comment-3
        return 'data:image/svg+xml,' + encodeURIComponent(this.svg);
    },

    get svg() {
        return (
            '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none">' +
            '<svg viewBox="0 0 100 100" preserveAspectRatio="xMidYMid slice">' +
            '<image width="100" height="100%" xlink:href="' +
            this.png +
            '" /></svg></svg>'
        );
    },

    get png() {
        return this.canvas.toDataURL();
    },

    get r() {
        return Math.sqrt(2) * this.size / 2;
    },

    // Paint the conical gradient on the canvas
    // Algorithm inspired from http://jsdo.it/akm2/yr9B
    paint: function() {
        var c = this.context;

        var radius = this.r;
        var x = this.size / 2;

        var stopIndex = 0; // The index of the current color
        var stop = this.stops[stopIndex],
            prevStop;

        var diff, t;

        // Transform coordinate system so that angles start from the top left, like in CSS
        c.translate(this.size / 2, this.size / 2);
        c.rotate(-90 * ConicGradient.deg);
        c.rotate(this.from * ConicGradient.deg);
        c.translate(-this.size / 2, -this.size / 2);

        for (var i = 0; i < 360; ) {
            if (i / 360 + ConicGradient.eps >= stop.pos) {
                // Switch color stop
                do {
                    prevStop = stop;

                    stopIndex++;
                    stop = this.stops[stopIndex];
                } while (stop && stop != prevStop && stop.pos === prevStop.pos);

                if (!stop) {
                    break;
                }

                var sameColor = prevStop.color + '' === stop.color + '' && prevStop != stop;

                diff = prevStop.color.map(function(c, i) {
                    return stop.color[i] - c;
                });
            }

            t = (i / 360 - prevStop.pos) / (stop.pos - prevStop.pos);

            var interpolated = sameColor
                ? stop.color
                : diff.map(function(d, i) {
                      var ret = d * t + prevStop.color[i];

                      return i < 3 ? ret & 255 : ret;
                  });

            // Draw a series of arcs, 1deg each
            c.fillStyle = 'rgba(' + interpolated.join(',') + ')';
            c.beginPath();
            c.moveTo(x, x);

            var theta = sameColor ? 360 * (stop.pos - prevStop.pos) : 0.5;

            var beginArg = i * ConicGradient.deg;
            beginArg = Math.min(360 * ConicGradient.deg, beginArg);

            // .02: To prevent empty blank line and corresponding moire
            // only non-alpha colors are cared now
            var endArg = beginArg + theta * ConicGradient.deg;
            endArg = Math.min(360 * ConicGradient.deg, endArg + 0.02);

            c.arc(x, x, radius, beginArg, endArg);

            c.closePath();
            c.fill();

            i += theta;
        }
    },
};

function colorPickerConicGradient() {
    return function ($scope, $element) {
        var size = $element[0].offsetWidth || 280;
        $element[0].style.background = new ConicGradient(size).toString();
    }
}

var colorPickerComponent = {
    bindings: {
        onSelect: '&',
        color: '<',
        onColorChange: '&',
        mouseScroll: '<',
        scrollSensitivity: '<'
    },
    controller: ColorPickerController,
    template: '\
        <div color-picker-conic-gradient class="color-palette"></div> \
        <div class="rotator" \
             color-picker-rotator \
             is-disabled="$ctrl.disabled" \
             mouse-scroll="$ctrl.mouseScroll" \
             scroll-sensitivity="$ctrl.scrollSensitivity" \
             on-rotate="$ctrl.onRotatorDrag(angle)" \
             angle="$ctrl.angle">\
            <div class="knob"></div>\
        </div> \
        <div class="color-shadow"></div> \
        <button type="button" class="color" ng-click="$ctrl.onColorSelClick()"></button> \
    '
};

ColorPickerController.$inject = ['$element', '$rootScope', 'ColorPickerService'];
function ColorPickerController($element, $rootScope, ColorPickerService) {
    var vm = this;

    this.disabled = false;
    this.colorModel = { hue: 0, saturation: 100, luminosity: 50, alpha: 1 };

    this.$onInit = $onInit;
    this.$onDestroy = $onDestroy;
    this.$onChanges = $onChanges;
    this.onColorSelClick = onColorSelClick;
    this.onRotatorDrag = onRotatorDrag;

    var wrapper, knob, colorSel, ripple, palette;

    ColorPickerService.subscribe('open', openColorPicker);
    ColorPickerService.subscribe('close', closeColorPicker);

    function $onInit() {
        wrapper = $element[0];
        knob = wrapper.querySelector('.knob');
        colorSel = wrapper.querySelector('.color');
        ripple = wrapper.querySelector('.color-shadow');
        palette = wrapper.querySelector('.color-palette');

        wrapper.addEventListener('keyup', _togglePalleteOnEnter);
        colorSel.addEventListener('animationend', _onColorSelAnimationEnd);
        knob.addEventListener('transitionend', _onKnobTransitionEnd);
        ripple.addEventListener('animationend', _onRippleAnimationEnd);
        palette.addEventListener('transitionend', _onPaletteTransitionEnd);

        wrapper.setAttribute('tabindex', 0);

        if (vm.color && vm.color.hue) {
            vm.angle = vm.color.hue;
            _updateColoredElements(vm.color.hue);
        } else {
            vm.angle = 0;
        }
    }

    function $onChanges(changeObj) {
        if (changeObj.color) {
            // on angular > 1.5.3 $onChanges is triggered once before $onInit and then again after $onInit
            if (colorSel && ripple) {
                vm.angle = vm.color.hue;
                _updateColoredElements(vm.color.hue);
            }
        }
    }

    function $onDestroy() {
        wrapper.removeEventListener('keyup', _togglePalleteOnEnter);
        colorSel.removeEventListener('animationend', _onColorSelAnimationEnd);
        knob.removeEventListener('transitionend', _onKnobTransitionEnd);
        ripple.removeEventListener('animationend', _onRippleAnimationEnd);
        palette.removeEventListener('transitionend', _onPaletteTransitionEnd);

        // clear circular child DOM node references to allow GC to collect them
        knob     = null; wrapper = null;
        colorSel = null; ripple = null;
        palette  = null;

        ColorPickerService.unsubscribe();
    }

    function onColorSelClick() {
        colorSel.classList.add('click-color');

        if (!vm.disabled) {
            ColorPickerService.publish('selected', vm.colorModel);
            vm.onSelect({ color: vm.colorModel });
            ColorPickerService.publish('hide', vm.colorModel);
            ripple.classList.add('color-shadow-animate');
        } else {
            ColorPickerService.publish('show', vm.colorModel);
            palette.classList.add('blur-palette-in');
            palette.classList.remove('blur-palette-out');
        }
    }

    function onRotatorDrag(angle) {
        vm.angle = angle;

        _updateColoredElements(angle);

        $rootScope.$applyAsync(function() {
            vm.onColorChange({ color: vm.colorModel });
        });
    }

    function openColorPicker() {
        if (vm.disabled) {
            ColorPickerService.publish('show', vm.colorModel);

            // showing palette will also show the knob
            palette.classList.add('blur-palette-in');
            palette.classList.remove('blur-palette-out');
        }
    }

    function closeColorPicker() {
        if (!vm.disabled) {
            ColorPickerService.publish('hide', vm.colorModel);

            // hiding knob will also hide the palette
            knob.classList.add('zoom-knob-out');
            knob.classList.remove('zoom-knob-in');
        }
    }

    function _togglePalleteOnEnter(ev) {
        if (ColorPickerService.isKey.enter(ev.key)) {
            onColorSelClick();
        }
    }

    function _updateColoredElements(angle) {
        var color = '';

        if (vm.color) {
            vm.color.hue = angle;
            color = 'hsla(' + vm.color.hue + ', ' + vm.color.saturation + '%, ' + vm.color.luminosity + '%, ' + vm.color.alpha + ')';
            vm.colorModel = vm.color;
        } else {
            color = 'hsla(' + angle + ', 100%, 50%, 1)';
            vm.colorModel = { hue: angle, saturation: 100, luminosity: 50, alpha: 1 };
        }

        colorSel.style.backgroundColor = color;
        ripple.style.borderColor = color;
    }

    function _onColorSelAnimationEnd() {
        if (vm.disabled) {
            knob.classList.add('zoom-knob-in');
            knob.classList.remove('zoom-knob-out');
        } else {
            knob.classList.add('zoom-knob-out');
            knob.classList.remove('zoom-knob-in');
        }

        colorSel.classList.remove('click-color');
    }

    function _onKnobTransitionEnd(ev) {
        // 'transitionend' fires for every transitioned property
        if (ev.propertyName === 'transform') {
            if (!vm.disabled) {
                palette.classList.add('blur-palette-out');
                palette.classList.remove('blur-palette-in');
            } else {
                vm.disabled = false;
                wrapper.classList.remove('disabled');
                ColorPickerService.publish('shown', vm.colorModel);
            }
        }
    }

    function _onRippleAnimationEnd() {
        ripple.classList.remove('color-shadow-animate');
    }

    function _onPaletteTransitionEnd(ev) {
        // 'transitionend' fires for every transitioned property
        if (ev.propertyName === 'transform') {
            if (vm.disabled) {
                knob.classList.add('zoom-knob-in');
                knob.classList.remove('zoom-knob-out');
            } else {
                vm.disabled = true;
                wrapper.classList.add('disabled');
                ColorPickerService.publish('hidden', vm.colorModel);
            }
        }
    }
}

var colorPickerModule = angular
    /**
     * @ngdoc module
     * @name color.picker.core
     * @description
     *
     * Angular Radial Color Picker
     */
    .module('color.picker.core', [])

    /**
     * @ngdoc service
     * @namespace Utilities
     * @name ColorPickerService
     * @module color.picker.core
     * @requires $rootScope
     *
     * @description
     * API for intercomponent comunication and color model conversions.
     *
     * @example
     * // Convert RGB color model to hexadecimal string
     * ColorPickerService.rgbToHex(255, 0, 0); // returns 'FF0000'
     */
    .service('ColorPickerService', ColorPickerService)

    /**
     * @ngdoc directive
     * @name colorPickerRotator
     * @module color.picker.core
     * @restrict A
     *
     * @private
     *
     * @param {expression} [onRotate]          Function to invoke when angle changes
     * @param {number}     [angle]             Angle to change the rotator at. A number between 0 and 359
     * @param {boolean}    [disabled]          Reference for the closed state of the picker. When the picker is closed disabled is true. Defaults to false
     * @param {boolean}    [mouseScroll]       Opt-in for using wheel event to rotate. Defaults to falsy value and the wheel event listener is not added
     * @param {number}     [scrollSensitivity] Amount of degrees to rotate the picker with keyboard and/or wheel. Defaults to 2 degrees
     *
     * @description
     * Provides rotation capabilities to any element. Also supports touch devices.
     * Although it's visible to other modules usage of this directive is discouraged by 3rd party users.
     * Changes in this directive are considered as breaking and are subject to change at any time.
     *
     * @example <div color-picker-rotator on-rotate="$ctrl.onRotate(angle)" angle="0" disabled="false" mouse-scroll="false" scroll-sensitivity="2"></div>
     */
    .directive('colorPickerRotator', colorPickerRotator)

    /**
     * @ngdoc directive
     * @name colorPickerConicGradient
     * @module color.picker.core
     * @restrict A
     *
     * @description
     * Generates a bitmap conic gradient to polyfill the CSS L4 conic-gradient().
     *
     * @example <div color-picker-conic-gradient></div>
     */
    .directive('colorPickerConicGradient', colorPickerConicGradient)

    /**
     * @ngdoc component
     * @name colorPicker
     * @module color.picker.core
     * @restrict E
     *
     * @param {expression} [onSelect]          A function to invoke when user selects a color
     * @param {Object}     [color]             HSLA color model. If provided will set the picker to the provided color
     *                                         Defaults to { hue: 0, saturation: 100, luminosity: 50, alpha: 1 }
     * @param {number}     color.hue           Value between 0 and 359
     * @param {number}     color.saturation    Value between 0 and 100
     * @param {number}     color.luminosity    Value between 0 and 100
     * @param {number}     color.alpha         Value between 0 and 1
     * @param {expression} [onColorChange]     A function to invoke when color is changed (i.e. on rotation).
     *                                         `on-color-change` is like `ng-change` to `ng-model` - triggered every time the color is changed
     * @param {boolean}    [mouseScroll]       Opt-in for using wheel event to rotate.
     *                                         Defaults to falsy value and the wheel event listener is not added
     * @param {number}     [scrollSensitivity] Amount of degrees to rotate the picker with keyboard and/or wheel. Defaults to 2 degrees
     *
     * @description
     * Material design radial color picker. Provides selecting a pure color via dragging the whole color wheel.
     * `color` is used to change programatically the active color in the picker. If it's not provided
     * the initial color defaults to red (0, 100%, 50%, 1).
     *
     * The `on-select` attribute is a function which is called when a user a user selects a color with the color
     * selector in the middle. The function is invoked only if the color picker is opened.
     *
     * For easier communication a set of events are provided that can even programatically open or close the picker
     * without interacting with the UI. All events are published/subscribed at the $rootScope so that sibling components
     * can subscribe to them too. This means that you can subscribe for an event on the $scope too.
     * All events carry the current color in the event data payload.
     *
     * `color-picker.show` - Fires when the color picker is about to show and *before* any animation is started.
     * `color-picker.shown` - Fires when the color picker is shown and has finished animating.
     *
     * `color-picker.selected` - Fires when a color is selected via the middle selector. Event is fired right before `hide`.
     *
     * `color-picker.hide` - Fires when the color picker is about to hide and *before* any animation is started.
     * `color-picker.hidden` - Fires when the color picker is hidden and has finished animating.
     *
     * `color-picker.open` - Programatically opens the color picker if it's not already open.
     * `color-picker.close` - Programatically closes the color picker if it's not already closed.
     *
     * @example <color-picker on-select="$ctrl.onSelect(color)" color="$ctrl.initialColor" on-color-change=""></color-picker>
     */
    .component('colorPicker', colorPickerComponent);

var main = colorPickerModule.name;

export default main;
